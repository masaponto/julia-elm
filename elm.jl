
using Base.Test

type Model
    a_vs::Array{Float64, 2}
    beta_vs::Array{Float64, 1}
end


function sigmoid(x::Float64)
    return 1 / (1 + exp(-x))
end


function add_bias(x::Array{Float64, 2})
    bs = fill(1,size(x)[1])
    return hcat(x,bs)
end

@test add_bias( [1.0 2.0; 3.0 4.0] ) == [1.0 2.0 1.0 ; 3.0 4.0 1.0]


function elm(X::Array{Float64, 2}, y::Array{Float64, 2}, hid_num::Int)

    x_vs = add_bias(X)
    a_vs = rand(size(x_vs)[2],hid_num) * 2 - 1

    h_t = pinv( map(sigmoid, x_vs * a_vs) )

    beta_vs = h_t * y

    return Model(a_vs, beta_vs)

end


function predict(model::Model, x::Array{Float64, 2})
    x_vs = add_bias(x)
    return sign(map(sigmoid, (x_vs * model.a_vs)) * model.beta_vs)
end


function xor()
    X = [1.0 1.0; 1.0 0.0; 0.0 1.0; 0.0 0.0]
    y = [1.0; -1.0; -1.0; 1.0]
    hid_num = 10
    model = elm(X, y, hid_num)
    r = predict(model, X)
    println(y)
    println(r)
end

function main()
    xor()
end

main()
